class Anthakshari 
    def Read_input_from_user
        input=gets                  # getting input from user
        array=input.split().uniq    # converting string into array without any duplicate elements
        play(array)
    end
    def play(array)
        final_array=Array.new
        final_array<< array[0]
        last_char_of_prev_word=array[0][-1] #storing last character of previous word used to compare with starting letter of other words
        index=0
        puts "Game Started"
        while(array.length()!=0)
            array.delete_at(index)   #after checking for the word delete that word in order to avoid duplication
            for word in array
                if (word.start_with? last_char_of_prev_word) && (!final_array.include? word) 
                    final_array<< word 
                    last_char_of_prev_word=word[-1]
                    index=array.find_index(word)
                    break
                end
            end
        end  
        print "#{final_array}\n"
        puts "Game Ended"
    end
end
a=Anthakshari.new
a.Read_input_from_user